import React from 'react'
import './about.css'
import ME from '../../assets/me-about.png'
import {GiGraduateCap} from 'react-icons/gi'
import {GiCannon} from 'react-icons/gi'
import {AiFillFolderOpen} from 'react-icons/ai'

const About = () => {
  return (
    <section id='about'>
      <h5>Get To Know</h5>
      <h2>About Me</h2>

      <div className="container about__container">
        <div className="about__me">
          <div className='about__me-image'>
            <img src={ME} alt="About Image"/>
          </div>
        </div>

        <div className='about__content'>
          <div className="about__cards">

            <article className='about__card'>
              <GiGraduateCap className='about__icon'/>
              <h5>Experience</h5>
              <small>Hack Reactor 19-Week Bootcamp</small>
            </article>

            <article className='about__card'>
              <GiCannon className='about__icon'/>
              <h5>Military</h5>
              <small>Hawaii National Guard Field Artillery Officer</small>
            </article>

            <article className='about__card'>
              <AiFillFolderOpen className='about__icon'/>
              <h5>Projects</h5>
              <small>15+ Completed</small>
            </article>

          </div>
            <p>
            Lorem ipsum dolor sit amet, 
            consectetur adipiscing elit. 
            Donec tincidunt elit quis metus vehicula ultrices.
            Vivamus rutrum neque at risus venenatis, id tincidunt orci pulvinar. 
            Nulla sed euismod ligula. Praesent ut lectus quis sem volutpat fermentum porttitor in nisl.
            Nullam facilisis efficitur felis vel auctor. Sed elit urna, pharetra sed lacinia quis, commodo eu dui. 
            Nam cursus bibendum diam, id tempor magna vulputate vitae. Nulla hendrerit consequat finibus.         
            </p>
          <a href='#contact' className='btn btn-primary'> Let's Talk</a>

        </div>
      </div>
    </section>
  )
}

export default About