import React from 'react'
import {AiOutlineLinkedin} from "react-icons/ai"
import {AiOutlineGitlab} from "react-icons/ai"
import {AiOutlineFacebook} from "react-icons/ai"

const HeaderSocials = () => {
  return (
    <div className='header__socials'>
        <a href="https://gitlab.com/otschlegelmilch" target="__blank"><AiOutlineGitlab/></a>
        <a href="https://www.linkedin.com/in/orion-schlegelmilch/" target="__blank"><AiOutlineLinkedin/></a>
        <a href="https://www.facebook.com/orion.schlegelmilch/" target="__blank"><AiOutlineFacebook/></a>
    </div>
  )
}

export default HeaderSocials